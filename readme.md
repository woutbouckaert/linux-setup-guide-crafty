# Ubuntu Linux setup for crafty

## Installing Ubuntu

## Setting up Ubuntu

After setting up Ubuntu, we'll take the following setup to setup and secure the server:

### Creating a sudoer worker user

The account `root` is the linux administrator user. The root user should not be used, a separate user should be used.

To create a new user, use:

```bash
adduser <username>
```

Follow the steps to make a user password and and user account. While typing the password, you will not see it update. This is normal, what you are typing is being recorded.

To make a user called example:

```bash
adduser example
```

After making a user, if you would like that user to have the ability to run administrative commands, we need to make them a `sudo` user. Sudo, "Super User DO," is the linux equivalent of running as administrator.

To make a user a `sudo` user, use:

```bash
usermod -aG sudo <username>
```

To add the user `example` to the sudo group, use:

```bash
usermod -aG sudo example
```

You can now run commands as administrator from your sudo user by adding `sudo` before the command you are attempting to run.

### Disable root account

It is best practice to disable the root account, as this account can run any commands on the system without a password entry.

Before disabling the root user. Ensure that you have a sudo user available to use for all maintenance tasks of the of the server.

#### Remove root password

Remove the password of the root user with

```bash
sudo passwd -d root
```

#### Disable sign-in for root user

Edit `/etc/passwd` with

```bash
sudo nano /etc/passwd
```

And modify the root user to have no login by modifying

```bash
root:x:0:0:root:/root:/bin/bash
```

To be

```bash
root:x:0:0:root:/root:/usr/sbin/nologin
```

After this edit, when attempting to sign in to the root user, you should see the following warning.

```bash
example@example:~$ sudo su
This account is currently not available.
```

### Setup ufw firewall

`ufw`, or Uncomplicated Firewall, is an easy way to manage the firewall on your server. Ufw, or any firewall, will prevent unauthorized connections to your server, you should always enable a firewall. Ufw comes pre-installed with Ubuntu and is recommended for beginners.

Note, that some cloud providers, including oracle, may require additional setup before ufw can be used.

You can view the status of ufw with `sudo ufw status`.

Before enabling enabling ufw, make sure that you allow ssh connections. If you do not, you will lock yourself out of your server. Allow ssh connections with:

```bash
sudo ufw allow ssh
```

Then enable your firewall with:

```bash
sudo ufw enable
```

By default, ufw will block all connections. You will need to allow each new type of connection to your server. This is done by allowing connections on specified ports. The basic syntax is:

```bash
sudo ufw allow <port>
```

If you want to allow connections to the minecraft on the default port of `25565`, you can use:

```bash
sudo ufw allow 25565
```

To allow connections to the Crafty web interface use:

```bash
sudo ufw allow 8443
```

At anytime you can check which ports are open, and if ufw is enabled with

```bash
sudo ufw status
```

The more advanced syntax, for allowing more specific connections is as follows:

```bash
sudo ufw allow from <ip or any> to <ip or any> proto <tcp/udp or any> comment 'Your comment here'
```

Portions:

- From: The source IP. `any` for all
- To: The destination IP. `any` for all
- Proto: TCP or UDP protocol, for both use `any`
- Comment: Optional comment you can add that will show up in `ufw status`

Example allowing udp connections from ip `1.1.1.1/8` with a comment:

```bash
sudo ufw allow from 1.1.1.1/8 to any proto udp comment 'My example ufw command``
```

## Linux for beginners

## Bonus security steps
